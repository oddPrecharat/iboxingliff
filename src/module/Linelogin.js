import axios from 'axios'
const liff = window.liff
const api_url = "https://node.mtf.in.th"
export default function Linelogin (callback) {
    liff.init(async (data) => {
        let getProfile = await liff.getProfile();
        let line = {
            displayName: getProfile.displayName,
            userId: getProfile.userId,
            pictureUrl: getProfile.pictureUrl,
            statusMessage: getProfile.statusMessage
        }
        axios.post(api_url + "/liff/login", {
            token: liff.getAccessToken()
          }).then(res => {
            let data = {
                token: liff.getAccessToken(),
                profile : res.data.data,
                line: line
            }
            if(res.status === 200){
              setTimeout(function(){ 
                document.getElementById("loading").style.display = 'none' 
              }, 1500);
              
            }
            console.log(res);
          return callback(data)
        });   
    });
    
}