import React from 'react'
import numeral from 'numeral'

const AmountSelect = ({ value, active, setAmount }) => {
    return (
        <div className="col amount-box">
            <label onClick={() => setAmount(value)}>
                <input type="radio" name="radio" value={value} />
                <div className={`amount box ${active}`}>
                    <span>{numeral(value).format('0,0')}</span>
                </div>
            </label>
        </div>
    )
}

export default AmountSelect