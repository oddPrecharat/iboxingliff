import React from 'react'

import avatarBg from '../assets/images/Asset_15.png'
import logoBg from '../assets/images/Asset_16.png'

const Player = ({ player }) => {

    return (

        <div className={player === 'playerOne' ? 'player-one' : 'player-two'}>

            {player === 'playerOne' &&
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <div className="payer-avatar" style={avatar}>
                                <div className="payer-logo" style={logo}>
                                </div>
                            </div>
                            <div>สถิติล่าสุด</div>
                            <div>ชนะ 100 KOs</div>
                            <div>แพ้ 3 KOs</div>
                            <div>เสมอ 1 KOs</div>
                        </div>
                        <div className="col">
                            <img src={require('../assets/images/d07a1c7d49.jpg')} height="20" alt="Fake Child EducationSite Label" />
                            <div>เพรชมณี</div>
                            <div>ศิษย์ชาญสิงห์</div>
                        </div>
                    </div>
                </div>
            }

            {player === 'playerTwo' &&
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <div className="text-bottom">
                                <img src={require('../assets/images/d07a1c7d49.jpg')} height="20" alt="Fake Child EducationSite Label" />
                                <div>เพรชมณี</div>
                                <div>ศิษย์ชาญสิงห์</div>
                            </div>
                        </div>
                        <div className="col">
                            <div>สถิติล่าสุด</div>
                            <div>ชนะ 100 KOs</div>
                            <div>แพ้ 3 KOs</div>
                            <div>เสมอ 1 KOs</div>
                            <div className="payer-avatar" style={avatar}>
                                <div className="payer-logo" style={logo}>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }

        </div>
    )
}


const avatar = {
    background: 'url(' + avatarBg + ') center',
    backgroundSize: 'cover'
}
const logo = {
    background: 'url(' + logoBg + ') center',
    backgroundSize: 'cover'
}


export default Player