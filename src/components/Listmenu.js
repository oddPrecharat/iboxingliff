import React from 'react'
import { connect } from 'react-redux'
import {  icon_arrow } from '../assets/images'
import { setLine } from '../reducers/appReducer'
const listMenu = ({name, path, props, state}) => {
    return (
        <div className="col-h-1-5" onClick={() => { handleSubmit(path, props, state)}}><div className="box-border-bottom">{name} <img src={icon_arrow} alt="baht" height="15" className="float-right"/></div></div>
    )
}
const handleSubmit = (path, props, state) => {
    props.setLine(state.profile)
    props.history.push(path)      
}
export default connect(null, { setLine })(listMenu)