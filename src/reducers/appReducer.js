export const SET_PROFILE = 'appRudcer/SET_PROFILE'
export const SET_BET = 'appRudcer/SET_BET'
export const SET_LINE = 'appRudcer/SET_LINE'

const initialState = {
    profile: {},
    bet: {
        ticket: 0,
        amount: 0,
        total: 0
    },
    line:{}
}

export default (state = initialState, action) => {
    switch (action.type) {

        case SET_PROFILE: {
            return {
                ...state,
                profile: action.payload
            }

        }

        case SET_BET: {
            return {
                ...state,
                bet: action.payload
            }
        }
        case SET_LINE: {
            return {
                ...state,
                line: action.payload
            }
        }
        default: {
            return state
        }

    }
}

export const setProfile = (data) => {
    return (dispatch) => {
        dispatch({
            type: SET_PROFILE,
            payload: data
        })
    }
}

export const setBet = (data) => {
    return (dispatch) => {
        dispatch({
            type: SET_BET,
            payload: data
        })
    }
}

export const setLine = (data) => {
    return (dispatch) => {
        dispatch({
            type: SET_LINE,
            payload: data
        })
    }
}

