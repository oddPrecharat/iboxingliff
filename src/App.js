import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { Home, Bet, Confirm, Complete, Menu, Profile, Live, Historical, Competition } from './pages'

const app = () => {
  return (
    <Router>
      <Route exact path="/" component={Home} />
      <Route path="/bet" component={Bet} />
      <Route path="/confirm" component={Confirm} />
      <Route path="/complete" component={Complete} />
      <Route path="/menu" component={Menu} />
      <Route path="/profile" component={Profile} />
      <Route path="/live" component={Live} />
      <Route path="/historical" component={Historical} />
      <Route path="/competition" component={Competition} />
      
    </Router>
  )
}

export default app
