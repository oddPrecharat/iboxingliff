import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { Player } from '../components'
import { setProfile } from '../reducers/appReducer'
import queryString from 'query-string'

const liff = window.liff

class Home extends Component {
    constructor(props) {
        super(props)

        this.state = {
            profile: {
                displayName: '',
                userId: 0,
                pictureUrl: '',
                statusMessage: ''
            }
        }
    }
    componentDidMount = () => {

        window.addEventListener('load', this.initLineLiff());
        let params = queryString.parse(this.props.location.search)
        if(params.teamId){
            let teamId = parseInt(params.teamId)
            let matchId = parseInt(params.matchId)
            let data = { name: 'Blue', age: 18, team : teamId, match : matchId }
            if(teamId == 1){
                data.name = 'Red';
            }else{
                data.name = 'Blue';
            }
            this.props.setProfile(data)
            console.log(teamId);
            this.props.history.push('/bet')                
        }
    }

    initLineLiff = () => {
        liff.init(async (data) => {
            let getProfile = await liff.getProfile();

            let profile = {
                displayName: getProfile.displayName,
                userId: getProfile.userId,
                pictureUrl: getProfile.pictureUrl,
                statusMessage: getProfile.statusMessage
            }
            this.setState({ ...this.state, profile });
        });
    }

    selectPlayer = (data) => {

        this.props.setProfile(data)
        this.props.history.push('/bet')

    }

    render() {
        return (
            <Fragment>
                <div className="home-background">
                    <div className="score-box">
                        <div className="score-number1">6</div>
                        <div className="score-number2">8</div>
                    </div>

                    <div className="time-box">
                        12 APR 15.30<p className="red">6' min</p>
                    </div>

                    <Player player={'playerOne'} />

                    <Player player={'playerTwo'} />

                </div>

                <div className="home-footer">
                    <div className="container">
                        <div className="row">
                            <div className="col">
                                <p style={{ 'color': '#ffffff' }} onClick={() => this.selectPlayer({ name: 'Red', age: 18, team : 1,  match : 3333})}>เล่นฝั่งแดง</p>

                            </div>
                            <div className="col" style={{ 'borderLeft': '2px solid' }}>

                                <p style={{ 'color': '#ffffff' }} onClick={() => this.selectPlayer({ name: 'Blue', age: 18, team : 2, match : 3333 })}>เล่นฝั่งน้ำเงิน</p>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }

}

const mapStateToProps = ({ appReducer }) => ({
    count: appReducer.count
})


export default connect(mapStateToProps, { setProfile })(Home)

