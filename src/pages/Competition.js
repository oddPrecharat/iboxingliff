import React from 'react'
import { connect } from 'react-redux'
const Competition = (props) => {
    return (
        <div className="container">
            <div className="row">
                <div className="col-12 text-center">
                    คู่มวยและราคา
                    <div className="row border-bottom-1 text-left">
                        <div className="col-10">
                            ชื่อมวย - คณะ
                        </div>
                        <div className="col-2">
                            ราคา
                        </div>
                    </div>
                </div>
                <div class="w-100"></div>
                <div className="col-12">
                    <div class="row border-bottom-1">
                        <div className="col-4 color-red">อรรถชัย</div>
                        <div className="col-6 color-red">ร.ร.กีฬาลําปาง</div>
                        <div className="col-2 color-red">5/4</div>
                        <div class="w-100"></div>
                        <div className="col-4 color-blue">เพชรมรกต</div>
                        <div className="col-6 color-blue">อู๊ดดอนเมือง</div>
                        <div className="col-2 color-blue">1/10</div>
                    </div>
                    <div class="row border-bottom-1">
                        <div className="col-4 color-red">อรรถชัย</div>
                        <div className="col-6 color-red">ร.ร.กีฬาลําปาง</div>
                        <div className="col-2 color-red">5/4</div>
                        <div class="w-100"></div>
                        <div className="col-4 color-blue">เพชรมรกต</div>
                        <div className="col-6 color-blue">อู๊ดดอนเมือง</div>
                        <div className="col-2 color-blue">1/10</div>
                    </div>
                    <div class="row border-bottom-1">
                        <div className="col-4 color-red">อรรถชัย</div>
                        <div className="col-6 color-red">ร.ร.กีฬาลําปาง</div>
                        <div className="col-2 color-red">5/4</div>
                        <div class="w-100"></div>
                        <div className="col-4 color-blue">เพชรมรกต</div>
                        <div className="col-6 color-blue">อู๊ดดอนเมือง</div>
                        <div className="col-2 color-blue">1/10</div>
                    </div>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = ({ appReducer }) => ({
    appReducer
})
export default connect(mapStateToProps)(Competition)