export { default as Home } from './Home'
export { default as Bet } from './Bet'
export { default as Confirm } from './Confirm'
export { default as Complete } from './Complete'
export { default as Menu } from './Menu'
export { default as Profile } from './Profile'
export { default as Live } from './Live'
export { default as Historical } from './Historical'
export { default as Competition } from './Competition'



