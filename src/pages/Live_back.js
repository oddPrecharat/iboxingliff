import React from 'react'
import { connect } from 'react-redux'
import avatarBg from '../assets/images/Asset_15.png'
import logoBg from '../assets/images/Asset_16.png'
const Live = (props) => {
    let state = props.appReducer;
    return (
        <div className="container">
            <div className="row color-white bg-team">
                <div className="col-12 text-center">รายการพนันสด</div>
                <div className="col">
                    <div className="row padding-top-botton-10">
                        <div className="col-6">
                            <div className="payer-avatar" style={avatar}>
                                <div className="payer-logo" style={logo}>
                                </div>
                            </div>
                        </div>
                        <div className="col-6">
                            เพรชมณี ศิษย์ชาญสิงห์
                        </div>
                    </div>
                </div>
                <div className="col">
                    <div className="row padding-top-botton-10">
                        <div className="col-6 text-right">
                            เพรชมณี ศิษย์ชาญสิงห์
                        </div>
                        <div className="col-6">
                            <div className="payer-avatar" style={avatar}>
                                <div className="payer-logo" style={logo}>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row box-live-score">
                <div className="col-12 text-center">
                    สถิติล่าสุด
                </div>
                <div className="col-12 text-center">
                    <div className="row">
                        <div className="col">
                            <div className="row" style={box_score}>
                                <div className="col color-green">ชนะ  <span className="text-score">10 OKs</span></div>
                                <div className="col color-red">แพ้  <span className="text-score">10 OKs</span></div>
                                <div className="col color-blue">เสมอ  <span className="text-score">10 OKs</span></div>
                            </div>
                        </div>
                        <div className="col">
                            <div className="row" style={box_score}>
                                <div className="col color-green">ชนะ  <span className="text-score">10 OKs</span></div>
                                <div className="col color-red">แพ้  <span className="text-score">10 OKs</span></div>
                                <div className="col color-blue">เสมอ  <span className="text-score">10 OKs</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-12 box-live-bg">
                    <div className="row">
                        <div className="col">วันที่/เวลา</div>
                        <div className="col">Bet ID</div>
                    </div>
                    <div className="row">
                        <div className="col">17/08/62 00:00 PM</div>
                        <div className="col">333333</div>
                    </div>
                    <div className="row">
                        <div className="col">แดง/ราคา</div>
                        <div className="col">ยอดเงิน</div>
                    </div>
                    <div className="row">
                        <div className="col">80000</div>
                        <div className="col">80000</div>
                    </div>
                </div>
                <div className="col-12 box-live-bg">
                    <div className="row">
                        <div className="col">วันที่/เวลา</div>
                        <div className="col">Bet ID</div>
                    </div>
                    <div className="row">
                        <div className="col">17/08/62 00:00 PM</div>
                        <div className="col">333333</div>
                    </div>
                    <div className="row">
                        <div className="col">แดง/ราคา</div>
                        <div className="col">ยอดเงิน</div>
                    </div>
                    <div className="row">
                        <div className="col">80000</div>
                        <div className="col">80000</div>
                    </div>
                </div>
                <div className="col-12 box-live-bg">
                    <div className="row">
                        <div className="col">วันที่/เวลา</div>
                        <div className="col">Bet ID</div>
                    </div>
                    <div className="row">
                        <div className="col">17/08/62 00:00 PM</div>
                        <div className="col">333333</div>
                    </div>
                    <div className="row">
                        <div className="col">แดง/ราคา</div>
                        <div className="col">ยอดเงิน</div>
                    </div>
                    <div className="row">
                        <div className="col">80000</div>
                        <div className="col">80000</div>
                    </div>
                </div>
                <div className="col-12 box-live-bg">
                    <div className="row">
                        <div className="col">วันที่/เวลา</div>
                        <div className="col">Bet ID</div>
                    </div>
                    <div className="row">
                        <div className="col">17/08/62 00:00 PM</div>
                        <div className="col">333333</div>
                    </div>
                    <div className="row">
                        <div className="col">แดง/ราคา</div>
                        <div className="col">ยอดเงิน</div>
                    </div>
                    <div className="row">
                        <div className="col">80000</div>
                        <div className="col">80000</div>
                    </div>
                </div>
            </div>
        </div>
    )
}
const box_score = {
    width: '100%',
    height: '50px',
    overflowY: 'hidden',
    overflowX: 'scroll',
    position: 'relative',
    flexWrap: 'inherit',
    position: 'relative'
}
const avatar = {
    background: 'url(' + avatarBg + ') center',
    backgroundSize: 'cover',
    width: '10vh',
    height: '10vh'
}
const logo = {
    background: 'url(' + logoBg + ') center',
    backgroundSize: 'cover',
    width: '3vh',
    height: '3vh'
}

const mapStateToProps = ({ appReducer }) => ({
    appReducer
})
export default connect(mapStateToProps)(Live)