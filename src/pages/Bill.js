import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import '../assets/styles/confirm.css'
import Success from '../assets/images/Asset_3.png'
import queryString from 'query-string'
import axios from 'axios'
import config from '../config'
const api_url = config.api

const Bill = (props) => {
    const [data, setData] = useState({
        _id: '',
        ticket_id: 0,
        message: '',
        team: {
            red: {
                total: 0,
                win: 0,
            },
            blue: {
                total: 0,
                win: 0,
            }
        },
        total: 0
    })
    useEffect(() => {
        window.addEventListener('load', loadData());
    });

    const loadData = () => {
        let query = queryString.parse(props.location.search)
        let Id = query.id
        axios.get(api_url + "/liff/bill/" + Id).then(res => {
            const persons = res.data
            if (persons.status == 200) {
                setData(persons.data.log)
                document.getElementById("loading").style.display = 'none'
            }
        });

    }
    return (
        <div>
            <div className="container">
                <div className="row">
                    <div className="col-12 text-center">
                        <div className="row">
                            <div className="col-4"></div>
                            <div className="col-4"><img src={Success} style={{ width: '100%' }} alt="Success" /></div>
                            <div className="col-4"></div>
                        </div>
                    </div>
                    <div className="col-12 text-center text-size-30-b">
                        ระบบรับการเล่นของคุณแล้ว
                    </div>
                    <div className="col-12 text-center text-size-30">
                        {data.message}
                    </div>
                    <div className="col-12 text-center">
                        Ticket ID :
                    </div>
                    <div className="w-100"></div>
                    <div className="col-12">
                        <div className="row">
                            <div className="col">
                                <div>
                                    <div>ยอดพนันฝั่งแดง</div>
                                    <span style={numberStyle}>฿ {data.team.red.total}</span>
                                </div>
                                <div>
                                    <div>ถ้าฝั่งแดงชนะ</div>
                                    <span style={numberStyle}>฿ {data.team.red.win}</span>
                                </div>
                            </div>
                            <div className="col">
                                <div>
                                    <div>ยอดพนันฝั่งน้ำเงิน</div>
                                    <span style={numberStyle}>฿ {data.team.blue.total}</span>
                                </div>
                                <div>
                                    <div>ถ้าฝั่งน้ำเงินชนะ</div>
                                    <span style={numberStyle}>฿ {data.team.blue.win}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="w-100"></div>
                    <div className="col text-center">
                        <div>ยอดพนันรวม</div>
                        <div style={totalStyle}>฿ {data.total}</div>
                        <div>12 APR 15:30</div>
                        <div>6'min</div>
                    </div>
                    <div className="w-100"></div>
                    <div className="col-12">
                        <div className="row">
                            <div className="col">
                                <div className="btn-close" onClick={() => handleBack()}>กลับไปหน้าแชท</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
const totalStyle = {
    fontSize: '2em',
    fontWeight: 'bold'
}
const numberStyle = {
    fontSize: '1.4em',
    fontWeight: 'bold'
}
const handleBack = () => {

}
const mapStateToProps = ({ appReducer }) => ({
    appReducer
})
export default connect(mapStateToProps, null)(Bill)