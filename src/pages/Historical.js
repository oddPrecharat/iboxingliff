import React from 'react'
import { connect } from 'react-redux'
const Historical = (props) => {
    return (
        <div className="container">
            <div className="row bg-historical">
                <div className="col-12 text-center">
                        รายการพนันย้อนหลัง
                </div>
                <div class="w-100"></div>
                <div className="col-12">
                    <div class="row">
                        <div className="col">วันที่</div>
                        <div className="col">เดิมพัน</div>
                        <div class="w-100"></div>
                        <div className="col">12/07/62</div>
                        <div className="col">80000</div>
                        <div class="w-100"></div>
                        <div className="col">ชนะ / แพ้</div>
                        <div className="col">ยอดเงินคงเหลือ</div>
                        <div class="w-100"></div>
                        <div className="col">100000</div>
                        <div className="col">80000</div>
                    </div>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = ({ appReducer }) => ({
    appReducer
})
export default connect(mapStateToProps)(Historical)