import React, { Component, Fragment } from 'react'
import { setLine } from '../reducers/appReducer'
import { connect } from 'react-redux'
import numeral from 'numeral'
import { Listmenu } from '../components'

const liff = window.liff
class Menu extends Component {
    constructor(props) {
        super(props)
        this.state = {
            profile: {
                displayName: '',
                userId: 0,
                pictureUrl: '',
                statusMessage: ''
            }
        }
    }
    componentDidMount = () => {
        window.addEventListener('load', this.initLineLiff());
    }
    initLineLiff = () => {
        liff.init(async (data) => {
            let getProfile = await liff.getProfile();
            let profile = {
                displayName: getProfile.displayName,
                userId: getProfile.userId,
                pictureUrl: getProfile.pictureUrl,
                statusMessage: getProfile.statusMessage
            }
            this.setState({ ...this.state, profile });
        });
    }
    menuData = [
        {
            name: "ข้อมูลส่วนตัว",
            path: "/profile"
        },
        {
            name: "รายการพนันสด",
            path: "/live"
        }, {
            name: "รายการพนันย้อนหลัง",
            path: "/historical"
        }, {
            name: "คู่มวยและราคา",
            path: "/competition"
        }
    ]
    render() {
        return (
            <Fragment>
                <div className="container">
                    <div className="row-h">
                        <div className="col-h-5 bg-profile">
                            <div className="row" style={{ marginTop: '5vh' }}>
                                <div className="col-12">รหัสผู้ใช้งาน <p>{this.state.profile.displayName}</p></div>
                                <div className="col-12">
                                    <div className="row" style={{ padding: '15px' }}>
                                        <div className="col bg-red text-center"> ยอดเงินคงเหลือ</div>
                                        <div className="col bg-blue text-center"> THB {numeral(6210).format('0,0')}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {this.menuData.map(value => { return (<Listmenu name={value.name} props={this.props} path={value.path} state={this.state} />) })}
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default connect(null, { setLine })(Menu)