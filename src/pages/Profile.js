import React from 'react'
import { connect } from 'react-redux'

const Profile = (props) => {
    let state = props.appReducer;
    return (
        <div className="container">
            <div className="row-h">
                <div className="col-h-content bg-profile">
                    <div className="row" style={{ marginTop: '3vh' }}>
                        <div className="col-12 text-center">ข้อมูลส่วนตัว <p>{state.profile.displayName}</p></div>
                        <div className="col-12">
                            <div className="row">
                                <div className="col">รหัสลูกค้า <p>213231</p></div>
                                <div className="col">รหัสเว็บ <p>213231</p></div>
                            </div>
                            <div className="row">
                                <div className="col">ชื่อ <p>213231</p></div>
                                <div className="col">นามสกุล <p>213231</p></div>
                            </div>
                            <div className="row">
                                <div className="col">ไลน์ไอดี <p>213231</p></div>
                                <div className="col">เบอร์โทรศัพท์ <p>213231</p></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-h-content">
                    <div className="row">
                        <div className="col">ยอดการเล่นสูงสุด</div>
                        <div className="col text-right">1000</div>
                    </div>
                </div>
                <div className="col-h-content">
                    <div className="row">
                        <div className="col">ยอดการเล่นต่ำสุด</div>
                        <div className="col text-right">1000</div>
                    </div>
                </div>
                <div className="col-h-content">
                    <div className="row">
                        <div className="col">ยอดเล่นสูงสุดต่อนาที</div>
                        <div className="col text-right">1000</div>
                    </div>
                </div>
            </div>
        </div>
    )
}
const mapStateToProps = ({ appReducer }) => ({
    appReducer
})
export default connect(mapStateToProps)(Profile)