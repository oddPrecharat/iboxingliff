import React from 'react'
import { connect } from 'react-redux'
import { setBet } from '../reducers/appReducer'
import '../assets/styles/confirm.css'

const Confirm = (props) => {


    return (
        <div className="bg-confirm" >
            <div className="container">
                <div className="row">
                    <div className="col-12 text-center">
                        <div className="div-box-x">X</div>
                    </div>
                    <div className="col-12 text-center text-size-30">
                        ราคาต่อรองเปลี่ยนไป !!
                    </div>
                    <div className="col-12 text-center margin-top-10">
                        <div className="row margin-bottom-10">
                            <div className="col text-center text-size-30-b">จาก 10/8</div>
                            <div className="col text-center text-size-30-b">12/8</div>
                        </div>
                        <div className="col margin-bottom-10">
                            <div className="row">
                                <div style={{ width :'80%', background : 'red'}} className="box-progress">12</div>
                                <div style={{ width :'20%' , background : 'blue'}} className="box-progress">8</div>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 margin-top-10">
                        <div className="row">
                            <div className="col">
                                <div className="btn-confirm" onClick={() => handleSubmit(props)}>ยอมรับราคานี้</div>
                            </div>
                            <div className="col">
                                <div className="btn-cancel" onClick={() => handleBack(props)}>ยกเลิก</div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

const handleSubmit = (props) => {


    props.history.push('/complete')

}

const handleBack = (props) => {
    props.history.push('/')
}
export default connect(null, { setBet })(Confirm)