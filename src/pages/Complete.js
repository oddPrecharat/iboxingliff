import React from 'react'
import { connect } from 'react-redux'
import '../assets/styles/confirm.css'
import numeral from 'numeral'
import Success from '../assets/images/Asset_3.png'
const liff = window.liff
const Complete = ({appReducer}) => {
    return (
        <div>
            <div className="container">
                <div className="row">
                    <div className="col-12 text-center text-size-30-b">
                        ระบบรับการเล่นของคุณแล้ว
                    </div>
                    <div className="col-12 text-center text-size-30">
                        จำนวนเงิน {numeral(appReducer.bet.amount*appReducer.bet.ticket).format('0,0')} บาท
                    </div>
                    <div className="col-12 text-center">
                        Ticket ID : {appReducer.profile.match}
                    </div>
                    <div className="col-12 text-center">
                        <div className="row">
                            <div className="col-2"></div>
                            <div className="col-8"><img src={Success} style={{ width : '200px'}} alt="Success" /></div>
                            <div className="col-2"></div>
                        </div>
                    </div>

                    <div className="col-12">
                        <div className="row">
                            <div className="col">
                                <div className="btn-close" onClick={() => handleBack()}>กลับไปหน้าแชท</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

const handleBack = () => {
    liff.closeWindow();
}
const mapStateToProps = ({ appReducer }) => ({
    appReducer
})
export default connect(mapStateToProps, null)(Complete)