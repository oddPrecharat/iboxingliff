import React, { useState } from 'react'
import { connect } from 'react-redux'
import { setBet } from '../reducers/appReducer'
import { Amount } from '../components'
import numeral from 'numeral'
import '../assets/styles/bet.css'

const amountData = [
    100, 500, 1000, 5000
]

const Bet = (props) => {
    const [ticket, setTicket] = useState(0)
    const [amount, setAmount] = useState(0)
    let state = props.appReducer;
    let bgTean = (state.profile.team === 1 ) ? "#cc0000" : "#0e47a0";
    (!state.profile.team) && props.history.push('/')
    return (
        <div className="bg" style={{backgroundColor : bgTean}}>
            <div className="container">
                <div className="row">
                    <div className="col text-center">
                        เงินพนัน
                        {/* <input className="box-number" type="number" value={ticket * amount} /> */}
                        <div className="box-number">
                            {numeral(ticket * amount).format('0,0')}
                        </div>
                    </div>
                    <div className="col text-center">
                        เงินที่คาดว่าจะได้
                        <div className="box-number bg-color-none">
                            1000
                        </div>
                    </div>
                    <div className="col-12 text-center margin-top-10">
                        <div className="row">
                            <div className="col-12 text-left">เลือกจำนวนเงิน</div>
                            {amountData.map((value, index) => {
                                return (
                                    <Amount
                                        key={index}
                                        value={value}
                                        active={value === amount ? 'active' : ''}
                                        setAmount={(data) => setAmount(data)}
                                    />

                                )
                            })
                            }
                        </div>

                    </div>
                    <div className="col-12 margin-top-10">
                        Total
                        <div className="row">
                            <div className="col">
                                <div className="total-box">{numeral(ticket * amount).format('0,0')}</div>
                            </div>
                            <div className="col">
                                <div className="row">
                                    <div className="col">
                                        <div className="btn-event" onClick={() => ticket > 0 && setTicket(ticket - 1)}> - </div>
                                    </div>
                                    <div className="col text-center">{ticket}</div>
                                    <div className="col">
                                        <div className="btn-event" onClick={() => setTicket(ticket + 1)}> + </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 margin-top-10">
                        <div className="row">
                            <div className="col">
                                <div className="btn-confirm" onClick={() => handleSubmit(ticket, amount, props)}>ยืนยัน</div>
                            </div>
                            <div className="col">
                                <div className="btn-cancel" onClick={() => handleBack(props)}>ยกเลิก</div>

                            </div>
                        </div>
                    </div>
                    {/* Bet match :{props.location.state.match} | {props.location.state.team} | Line user : */}
                </div>
            </div>
        </div>
    )
}
const handleSubmit = (ticket, amount, props) => {

    let data = {
        ticket: ticket,
        amount: amount,
        total: ticket * amount
    }

    props.setBet(data)

    props.history.push('/confirm')

}

const handleBack = (props) => {
    props.history.push('/')
}
const mapStateToProps = ({ appReducer }) => ({
    appReducer
})
export default connect(mapStateToProps, { setBet })(Bet)